package com;

import java.nio.file.Path;

public class Config {
    public Path inputFilesPath;
    public Path jdkJrePath;
    public Path packagedAppDestination;
    public String appName;
    public String appMainClass;
    public String appMainJar;
    public String type = "app-image";
    public boolean winConsole;
    public String appVersion = "1.0";
    public Path icon;
}
