package com;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class CreatePackagePanel extends JPanel {

    private JFileChooser jfcInputFiles = new JFileChooser(System.getProperty("user.dir"));
    private JFileChooser jfcJdkJre = new JFileChooser(System.getProperty("java.home"));
    private JFileChooser jfcMainJar = new JFileChooser();
    private JFileChooser jfcPackageAppDestination = new JFileChooser(System.getProperty("user.dir"));
    private JFileChooser jfcIcon = new JFileChooser(System.getProperty("user.dir"));
    private JFileChooser jfcConfig = new JFileChooser(System.getProperty("user.dir"));

    private Path inputFilesPath;
    private Path jdkJrePath;
    private Path packagedAppDestination;
    private String appName;
    private String appMainClass;
    private String appMainJar;
    private String type = "app-image";
    private boolean winConsole;
    private String appVersion = "1.0";
    private Path icon;
    private String overwriteCustom;

    private List<Runnable> configChangeListener = new ArrayList<>();

    public CreatePackagePanel() {
        super(new BorderLayout());

        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        add(toolBar, BorderLayout.NORTH);

        toolBar.add(new AbstractAction("New Config") {
            @Override
            public void actionPerformed(ActionEvent e) {
                inputFilesPath = Paths.get(System.getProperty("user.dir"));
                jdkJrePath = Paths.get(System.getProperty("java.home"));
                packagedAppDestination = Paths.get(System.getProperty("user.dir"));
                appName = "New App";
                appMainClass = "";
                appMainJar = "";
                type = "app-image";
                winConsole = false;
                appVersion = "1.0";
                icon = null;
                overwriteCustom = null;
                fireConfigChanged();
            }
        });
        toolBar.add(new AbstractAction("Save Config") {
            @Override
            public void actionPerformed(ActionEvent e) {
                var props = new Properties();
                props.setProperty("inputFilesPath", inputFilesPath.toString());
                props.setProperty("jdkJrePath", jdkJrePath.toString());
                props.setProperty("packagedAppDestination", packagedAppDestination.toString());
                props.setProperty("appName", appName);
                props.setProperty("appMainClass", appMainClass);
                props.setProperty("appMainJar", appMainJar);
                props.setProperty("type", type);
                props.setProperty("winConsole", Boolean.toString(winConsole));
                props.setProperty("appVersion", appVersion);
                props.setProperty("icon", icon == null ? "" : icon.toString());
                props.setProperty("overwriteCustom", overwriteCustom == null ? "" : overwriteCustom);

                jfcConfig.showSaveDialog(CreatePackagePanel.this);
                try {
                    props.store(Files.newBufferedWriter(jfcConfig.getSelectedFile().toPath()), null);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        });
        toolBar.add(new AbstractAction("Load Config") {
            @Override
            public void actionPerformed(ActionEvent e) {
                jfcConfig.showOpenDialog(CreatePackagePanel.this);
                Properties properties = new Properties();
                try {
                    properties.load(Files.newBufferedReader(jfcConfig.getSelectedFile().toPath()));
                    inputFilesPath = Paths.get(properties.getProperty("inputFilesPath"));
                    jdkJrePath = Paths.get(properties.getProperty("jdkJrePath"));
                    packagedAppDestination = Paths.get(properties.getProperty("packagedAppDestination"));
                    appName = properties.getProperty("appName");
                    appMainClass = properties.getProperty("appMainClass");
                    appMainJar = properties.getProperty("appMainJar");
                    type = properties.getProperty("type");
                    winConsole = Boolean.parseBoolean(properties.getProperty("winConsole"));
                    appVersion = properties.getProperty("appVersion");
                    icon = properties.getProperty("icon").isEmpty() ? null : Paths.get(properties.getProperty("icon"));
                    overwriteCustom = properties.getProperty("overwriteCustom") == null || properties.getProperty("overwriteCustom").isEmpty() ? null : properties.getProperty("overwriteCustom");

                    fireConfigChanged();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            }
        });

        toolBar.addSeparator();
        toolBar.add(new AbstractAction("Start Packaging") {
            @Override
            public void actionPerformed(ActionEvent e) {
                startPackaging();
            }
        });

        var configPanel = new JPanel(new GridLayout(0, 2));
        configPanel.add(new JLabel("Application Name"));
        configPanel.add(new JButton(new AbstractAction("...") {
            {
                addConfigChangeListener(() -> putValue(AbstractAction.NAME, appName));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                appName = JOptionPane.showInputDialog(CreatePackagePanel.this, "Application Name", appName);
                putValue(AbstractAction.NAME, appName);
            }
        }));

        configPanel.add(new JLabel("Application Version"));
        configPanel.add(new JButton(new AbstractAction("...") {
            {
                addConfigChangeListener(() -> putValue(AbstractAction.NAME, appVersion));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                appVersion = JOptionPane.showInputDialog(CreatePackagePanel.this, "Application Version", appVersion);
                putValue(AbstractAction.NAME, appVersion);
            }
        }));

        configPanel.add(new JLabel("Application Icon"));
        configPanel.add(new JButton(new AbstractAction("...") {
            {
                addConfigChangeListener(() -> putValue(AbstractAction.NAME, icon != null ? icon.toString() : "..."));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                jfcIcon.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                jfcIcon.showOpenDialog(CreatePackagePanel.this);
                var path = jfcIcon.getSelectedFile().toPath();
                putValue(AbstractAction.NAME, path.toString());
                icon = path;
            }
        }));

        configPanel.add(new JLabel("Input Files (jars):"));
        configPanel.add(new JButton(new AbstractAction("...") {
            {
                addConfigChangeListener(() -> putValue(AbstractAction.NAME, inputFilesPath.toString()));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                jfcInputFiles.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                jfcInputFiles.showOpenDialog(CreatePackagePanel.this);
                var path = jfcInputFiles.getSelectedFile().toPath();
                putValue(AbstractAction.NAME, path.toString());
                inputFilesPath = path;
            }
        }));

        configPanel.add(new JLabel("JDK/JRE:"));
        configPanel.add(new JButton(new AbstractAction("...") {
            {
                addConfigChangeListener(() -> putValue(AbstractAction.NAME, jdkJrePath.toString()));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                jfcJdkJre.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                jfcJdkJre.setCurrentDirectory(new File(System.getProperty("java.home")));
                jfcJdkJre.showOpenDialog(CreatePackagePanel.this);
                var path = jfcJdkJre.getSelectedFile().toPath();
                putValue(AbstractAction.NAME, path.toString());
                jdkJrePath = path;
            }
        }));
        configPanel.add(new JLabel("Main Jar"));
        configPanel.add(new JButton(new AbstractAction("...") {
            {
                addConfigChangeListener(() -> putValue(AbstractAction.NAME, appMainJar));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                jfcMainJar.setFileSelectionMode(JFileChooser.FILES_ONLY);
                jfcMainJar.setCurrentDirectory(inputFilesPath.toFile());
                jfcMainJar.showOpenDialog(CreatePackagePanel.this);
                appMainJar = jfcMainJar.getSelectedFile().toPath().getFileName().toString();
                putValue(AbstractAction.NAME, appMainJar);
            }
        }));
        configPanel.add(new JLabel("Main Class"));
        configPanel.add(new JButton(new AbstractAction("...") {
            {
                addConfigChangeListener(() -> putValue(AbstractAction.NAME, appMainClass));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                appMainClass = JOptionPane.showInputDialog(CreatePackagePanel.this, "Main Class", appMainClass);
                putValue(AbstractAction.NAME, appMainClass);
            }
        }));
        configPanel.add(new JLabel("Packaged App Destination:"));
        configPanel.add(new JButton(new AbstractAction("...") {
            {
                addConfigChangeListener(() -> putValue(AbstractAction.NAME, packagedAppDestination.toString()));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                jfcPackageAppDestination.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                jfcPackageAppDestination.setCurrentDirectory(new File(System.getProperty("user.dir")));
                jfcPackageAppDestination.showOpenDialog(CreatePackagePanel.this);
                var path = jfcPackageAppDestination.getSelectedFile().toPath();
                putValue(AbstractAction.NAME, path.toString());
                packagedAppDestination = path;
            }
        }));

        var typeCombo = new JComboBox<>(new DefaultComboBoxModel<>(new String[]{"app-image", "msi", "rpm", "deb", "pkg", "dmg"}));
        typeCombo.addActionListener(e -> type = (String) typeCombo.getSelectedItem());
        configPanel.add(new JLabel("Type"));
        configPanel.add(typeCombo);

        configPanel.add(new JCheckBox(new AbstractAction("Win Console") {

            @Override
            public void actionPerformed(ActionEvent e) {
                JCheckBox checkBox = (JCheckBox) e.getSource();
                winConsole = checkBox.isSelected();
            }
        }) {
            {
                addConfigChangeListener(() -> setSelected(winConsole));
            }
        });
        configPanel.add(new JLabel(""));


        var wrapper = new JPanel(new BorderLayout());
        wrapper.add(configPanel, BorderLayout.NORTH);
        add(wrapper);

        var southPanel = new JPanel(new BorderLayout());
        add(southPanel, BorderLayout.SOUTH);
        southPanel.add(new JScrollPane(new JTextArea() {
            {
                setRows(10);
                addConfigChangeListener(() -> setText(overwriteCustom));
                getDocument().addDocumentListener(new DocumentListener() {
                                                      @Override
                                                      public void insertUpdate(DocumentEvent e) {
                                                          overwriteCustom = getText();
                                                      }

                                                      @Override
                                                      public void removeUpdate(DocumentEvent e) {
                                                          overwriteCustom = getText();
                                                      }

                                                      @Override
                                                      public void changedUpdate(DocumentEvent e) {
                                                          overwriteCustom = getText();
                                                      }
                                                  }
                );
            }
        }));
        southPanel.add(new JLabel(
                """
                        <html>
                        Here you can overwrite any JPackage option.<br>
                        Put option and value on separate lines, for example:<br>
                        --name<br>
                        "Test"<br>
                        This will add the option --name "Test"<b>
                        </html>
                        """
        ), BorderLayout.SOUTH);


    }

    private void startPackaging() {
        if (packagedAppDestination.equals(inputFilesPath))
            return;

        try {
            deletePackagedAppDirectory();
            //createPackagedAppDirectory();
            runJPackage();
            openPackagedApp();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void createPackagedAppDirectory() throws IOException {
        Files.createDirectories(packagedAppDestination);
    }

    private void openPackagedApp() throws IOException {
        Desktop.getDesktop().browse(packagedAppDestination.toUri());
    }

    private void runJPackage() throws IOException, InterruptedException {
        List<String> command = new ArrayList<>();

        command.add(jdkJrePath.resolve("bin").resolve("jpackage.exe").toString());
        command.add("--input");
        command.add(inputFilesPath.toString());
        command.add("--name");
        command.add(appName);
        command.add("--main-jar");
        command.add(appMainJar);
        command.add("--main-class");
        command.add(appMainClass);
        command.add("--type");
        command.add(type);
        command.add("--app-version");
        command.add(appVersion);
        if (winConsole) command.add("--win-console");
        if (icon != null) {
            command.add("--icon");
            command.add(icon.toString());
        }

        if (overwriteCustom != null) {
            Scanner sc = new Scanner(overwriteCustom);
            String line;
            while (sc.hasNextLine() && (line = sc.nextLine()) != null) {
                command.add(line);
            }
        }


        var pb = new ProcessBuilder()
                .directory(packagedAppDestination.toFile())
                .inheritIO()
                .command(command);
        System.out.println("JPACKAGE: " + String.join(" ", pb.command()));
        var process = pb.start();

        var ret = process.waitFor();
        System.out.println("JPACKAGE FINISHED: " + ret);

        System.out.println(process.info().command().orElse(""));
        System.out.println(process.info().commandLine().orElse(""));

    }

    private void deletePackagedAppDirectory() throws IOException {

        Files.walkFileTree(packagedAppDestination.resolve(appName), new FileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                //var userPrincipal = FileSystems.getDefault().getUserPrincipalLookupService().lookupPrincipalByName(System.getProperty("user.name"));
                //if (userPrincipal!=null){
                //    Files.setOwner(file, userPrincipal);
                //}
                file.toFile().setWritable(true);
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                if (!dir.equals(packagedAppDestination)) {
                    Files.delete(dir);
                }
                return FileVisitResult.CONTINUE;
            }
        });
    }

    private void addConfigChangeListener(Runnable listener) {
        configChangeListener.add(listener);
    }

    private void fireConfigChanged() {
        for (Runnable runnable : configChangeListener) {
            SwingUtilities.invokeLater(runnable::run);
        }
    }
}
