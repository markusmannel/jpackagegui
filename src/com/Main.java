package com;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    private static String version = "1.0";

    public static void main(String[] args) {

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                var bout = new ByteArrayOutputStream();
                var pw = new PrintStream(bout);
                e.printStackTrace(pw);
                pw.flush();
                try {
                    Files.writeString(Paths.get("error.txt"), bout.toString());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        JFrame frame = new JFrame("JPackageGUI " + version);
        frame.setPreferredSize(new Dimension(1280,720));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        frame.add(new CreatePackagePanel());

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }
}
